(defproject polarize-fakenews-model "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"] [gorilla-plot "0.1.4"] [anglican "1.1.0"]]
  :plugins [[org.clojars.benfb/lein-gorilla "0.6.0"] [javax.xml.bind/jaxb-api "2.4.0-b180830.0359"]]
  :repl-options {:init-ns polarize-fakenews-model.core})
