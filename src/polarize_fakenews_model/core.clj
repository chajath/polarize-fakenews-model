(ns polarize-fakenews-model.core
  (:use [anglican emit runtime core]))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(defrecord News [politics truth])
(defrecord State [politics analytic])

(with-primitive-procedures [News State ->State]
  ;; We define a series of media landscape models. A media landscape model produces a news item on each iteration.

  (defm ^News premium-centrist-media []
    "Premium mainstream media is a centrist media with high truth value."
    (let [politics (sample (normal 0 0.5))
          truth (sample (normal 0.8 0.2))]
      {:politics politics :truth truth}))

  (defm ^News premium-partisan-media []
    "Premium partisan media is a biased (equally split between left and right) with high truth value."
    (let [politics (if (sample (flip 0.5))
                     (sample (normal -0.7 0.3))
                     (sample (normal 0.7 0.3)))
          truth (sample (normal 0.8 0.2))]
      {:politics politics :truth truth}))

  (defm ^News fakenews-partisan-media []
    "Fakenews partisan media is a highly biased media with questionable truth value."
    (let [politics (if (sample (flip 0.5))
                     (sample (normal -0.9 0.1))
                     (sample (normal 0.9 0.1)))
          truth (sample (normal 0.4 0.5))
          ] {:politics politics :truth truth}))

  (defm ^News mixed-media [pcm ppm fpm]
    (let [choice (sample (uniform-continuous 0 (+ pcm ppm fpm)))]
      (cond
        (< choice pcm) (premium-centrist-media)
        (< choice (+ pcm ppm)) (premium-partisan-media)
        :else (fakenews-partisan-media))))

  (defm news-processor [^State state ^News news]
    "A model that takes in current politics and the news and returns target political value of the news"
    (let [
          motivational-discount (* 0.2
                                   (pow
                                     0.2
                                     (abs
                                       (- (:politics state) (:politics news)))))
          is-true (>
                    (sample (uniform-continuous 0 (max 0.00001 (:truth news))))
                    (sample (uniform-continuous 0 (max 0.00001 (- (:analytic state) motivational-discount)))))]
      (if is-true (:politics news)
                  (- (:politics news)))))

  ;; We define root-level model.

  (defquery model-runner [[pcm ppm fpm how-many]]
    (let [politics (sample (normal 0 1))
          analytics (sample (uniform-continuous 0.5 1))
          likelihood (normal politics 0.25)]

      (loop [i how-many]
        (when (> i 0)
          (observe likelihood
                   (news-processor
                     (->State politics analytics)
                     (mixed-media pcm ppm fpm)))
          (recur (dec i))))

      politics)))

(defn run-me [args sample-size] (->> (doquery :plmh model-runner [args] :number-of-particles 5000)
                                     (take sample-size)
                                     (map :result)))
